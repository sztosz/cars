from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django import forms

from ess.models import ValidationScript, ECU


class EcuDumpUploadForm(forms.Form):
    file = forms.FileField(label='Send unmodified file')
    name = forms.CharField(label='Name for file')
    script = forms.IntegerField(widget=forms.HiddenInput())

    def clean(self):
        cleaned_data = super().clean()
        file = cleaned_data.get('file')
        name = cleaned_data.get('name')
        script_pk = cleaned_data.get('script')
        if not file or not name or not script_pk:
            return cleaned_data
        else:
            validation_script = ValidationScript.objects.get(pk=script_pk)
            temp_env = {}
            exec(validation_script.script, temp_env)
            sw, error = temp_env['validate'](file.read())
            if error == 'length':
                raise forms.ValidationError(
                    'File length is incorrect. Make sure you\'re uploading proper file or contact support.')
            elif error == 'sw':
                raise forms.ValidationError('Your SW is not in our database. Please contact support.')
            else:
                cleaned_data['sw'] = sw
        return cleaned_data

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-2'
        self.helper.field_class = 'col-lg-10'
        self.helper.add_input(Submit('submit', 'Send'))


class OrgEcuDumpUploadForm(forms.Form):
    file = forms.FileField(label='Send Original file')
    ecu = forms.ModelChoiceField(queryset=ECU.objects.all().select_related().order_by(
        'engine__chassis__brand__name', 'engine__chassis__name', 'engine__name'), label='ECU')

    sw = forms.CharField(label='SW', required=False)
    hw = forms.CharField(label='HW', required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-2'
        self.helper.field_class = 'col-lg-10'
        self.helper.add_input(Submit('submit', 'Send'))


class OrgEcuDumpModifyForm(OrgEcuDumpUploadForm):
    file = forms.FileField(label='Send Original file', required=False)


class EcuDumpModifyForm(forms.Form):
    def __init__(self, *args, **kwargs):
        mods = kwargs.pop('mods')
        super().__init__(*args, **kwargs)

        for mod in mods:
            self.fields[mod.name] = forms.BooleanField(required=False, label=mod.name, widget=forms.CheckboxInput)

        self.helper = FormHelper()
        self.helper.add_input(Submit('submit', 'Send'))


class OrgEcuDumpSearchForm(forms.Form):
    sw = forms.CharField(label='SW', required=False)
    hw = forms.CharField(label='HW', required=False)
    brand = forms.CharField(label='Brand', required=False)
    chassis = forms.CharField(label='Chassis', required=False)
    engine = forms.CharField(label='Engine', required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.form_class = 'form-inline'
        self.helper.add_input(Submit('submit', 'Search'))
