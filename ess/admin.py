from django.contrib import admin
from django.core.urlresolvers import reverse_lazy
from django.utils.safestring import mark_safe

from .models import Brand, Chassis, Engine, ECU, EcuMessage, Modification, ModScript, EcuDump, SoftwareVersion, \
    ValidationScript


@admin.register(Brand)
class BrandAdmin(admin.ModelAdmin):
    list_display = ('name', 'id')


@admin.register(Chassis)
class ChassisAdmin(admin.ModelAdmin):
    list_select_related = True
    list_display = ('name', 'brand_name', 'id')
    list_filter = ['brand__name']

    def brand_name(self, obj):
        return obj.brand.name


@admin.register(Engine)
class EngineAdmin(admin.ModelAdmin):
    list_select_related = True
    list_display = ('name', 'brand_name', 'chassis_name', 'id',)
    list_filter = ['chassis__brand__name', 'chassis__name']

    def brand_name(self, obj):
        return obj.chassis.brand.name

    def chassis_name(self, obj):
        return obj.chassis.name


@admin.register(ECU)
class EcuAdmin(admin.ModelAdmin):
    list_select_related = True
    list_display = ('name', 'brand_name', 'chassis_name', 'engine_name', 'id',)
    list_filter = ['engine__chassis__brand__name', 'engine__chassis__name', 'engine__name']

    def brand_name(self, obj):
        return obj.engine.chassis.brand.name

    def chassis_name(self, obj):
        return obj.engine.chassis.name

    def engine_name(self, obj):
        return obj.engine.name

    def get_queryset(self, request):
        return super().get_queryset(request).select_related()


admin.site.register(EcuMessage)
admin.site.register(Modification)


@admin.register(ModScript)
class ModScriptAdmin(admin.ModelAdmin):
    list_display = ('name', 'id', 'modification', 'ecus')
    filter_horizontal = ('ecu', 'sw')
    search_fields = ('name',)

    def ecus(self, obj):
        return '; '.join([ecu.__str__() for ecu in obj.ecu.all()])


@admin.register(EcuDump)
class EcuDumpAdmin(admin.ModelAdmin):
    list_select_related = True
    list_display = ('name', 'user', 'date', 'ecu', 'sw', 'modified', 'messages', 'file_download')
    fields = ('date', 'file_download', 'name', 'user', 'ecu', 'sw', 'modified', 'parent', 'messages')
    readonly_fields = ('date', 'file_download')
    list_filter = ['modified']
    search_fields = ('name', 'user__username')

    def get_queryset(self, request):
        dumps = super().get_queryset(request).defer('file').select_related()
        return dumps

    def file_download(self, obj):
        return mark_safe(
            '<a href="%s" class="link">Download</a>' % reverse_lazy('download-ecu-mod', kwargs={'pk': obj.id}))

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        if db_field.name == "ecu":
            kwargs["queryset"] = ECU.objects.all().select_related()
        if db_field.name == "sw":
            kwargs["queryset"] = SoftwareVersion.objects.all().select_related()

        return super().formfield_for_foreignkey(
            db_field, request, **kwargs)


@admin.register(SoftwareVersion)
class SoftwareVersionAdmin(admin.ModelAdmin):
    list_select_related = True
    search_fields = (
        'name', 'ecu__engine__chassis__brand__name', 'ecu__engine__chassis__name', 'ecu__engine__name', 'ecu__name'
    )
    list_filter = ['ecu__engine__chassis__brand__name', 'ecu__engine__chassis__name', 'ecu__engine__name', 'ecu__name']


@admin.register(ValidationScript)
class ValidationScriptAdmin(admin.ModelAdmin):
    list_select_related = True
    list_display = ('name', 'ecu')
