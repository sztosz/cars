# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-02-18 20:22
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ess', '0021_auto_20160211_1932'),
    ]

    operations = [
        migrations.CreateModel(
            name='EcuMessage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('message', models.TextField()),
            ],
        ),
        migrations.AddField(
            model_name='ecu',
            name='message',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='ess.EcuMessage'),
        ),
    ]
