# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-02-22 18:43
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ess', '0022_auto_20160218_2022'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ecu',
            name='message',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='ess.EcuMessage'),
        ),
    ]
