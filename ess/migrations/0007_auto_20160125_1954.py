# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-01-25 19:54
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ess', '0006_ecudump'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ecudump',
            name='error',
            field=models.CharField(blank=True, max_length=50),
        ),
        migrations.AlterField(
            model_name='ecudump',
            name='message',
            field=models.CharField(blank=True, max_length=50),
        ),
    ]
