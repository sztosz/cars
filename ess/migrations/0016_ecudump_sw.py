# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-02-01 20:19
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ess', '0015_auto_20160131_2252'),
    ]

    operations = [
        migrations.AddField(
            model_name='ecudump',
            name='sw',
            field=models.CharField(default='NO SW DELETE ME', max_length=20),
            preserve_default=False,
        ),
    ]
