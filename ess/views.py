from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.views.generic import ListView, View, FormView, DeleteView

from .forms import EcuDumpUploadForm, EcuDumpModifyForm, OrgEcuDumpUploadForm, OrgEcuDumpSearchForm, \
    OrgEcuDumpModifyForm
from .models import Brand, Chassis, Engine, ECU, EcuDump, SoftwareVersion, EcuDumpPayload, OrgEcuDump, OrgEcuDumpPayload


class BrandListView(ListView):
    model = Brand


class ChassisListView(ListView):
    model = Chassis

    def get_queryset(self):
        return Chassis.objects.filter(brand__name=self.kwargs['brand']).select_related()


class EngineListView(ListView):
    model = Engine

    def get_queryset(self):
        return Engine.objects.filter(
            chassis__brand__name=self.kwargs['brand']).filter(
            chassis__name=self.kwargs['chassis']).select_related()


class ECUListView(ListView):
    model = ECU

    def get_queryset(self):
        return ECU.objects.filter(
            engine__chassis__brand__name=self.kwargs['brand']).filter(
            engine__chassis__name=self.kwargs['chassis']).filter(
            engine__name=self.kwargs['engine']
        ).select_related()


class EcuDumpUploadView(LoginRequiredMixin, FormView):
    form_class = EcuDumpUploadForm
    template_name = 'ess/ecu_dump_upload.html'
    success_url = reverse_lazy('file-list')

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            software_version = SoftwareVersion.objects.filter(ecu=self.kwargs['ecu'], name=form.cleaned_data.get('sw'))
            if software_version:
                request.FILES['file'].seek(0)
                EcuDump.objects.create(user=request.user, ecu=self.kwargs['ecu'], sw=software_version[0],
                                       name=form.cleaned_data.get('name'), modified=False,
                                       file=EcuDumpPayload.objects.create(content=form.cleaned_data.get('file').read()))
                return self.form_valid(form)
            else:
                messages.warning(request, 'There was an internal error. Please contact support about your SW.')
        return self.form_invalid(form)

    def get_initial(self):
        self.kwargs['ecu'] = ECU.objects.filter(
            engine__chassis__brand__name=self.kwargs['brand']).filter(
            engine__chassis__name=self.kwargs['chassis']).filter(
            engine__name=self.kwargs['engine']).filter(
            name=self.kwargs['ecu']).select_related().get()
        script_id = self.kwargs['ecu'].validationscript.id
        return {'script': script_id}

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['ecu'] = self.kwargs['ecu']
        return context


class OrgEcuDumpUploadView(UserPassesTestMixin, FormView):
    form_class = OrgEcuDumpUploadForm
    template_name = 'ess/org_ecu_dump_upload.html'
    success_url = reverse_lazy('org-ecu-list')

    def test_func(self):
        return self.request.user.is_superuser

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            OrgEcuDump.objects.create(ecu=form.cleaned_data['ecu'], sw=form.cleaned_data['sw'],
                                      hw=form.cleaned_data['hw'],
                                      size=form.cleaned_data.get('file').size,
                                      file=OrgEcuDumpPayload.objects.create(
                                          content=form.cleaned_data.get('file').read()))
            return self.form_valid(form)
        return self.form_invalid(form)


class OrgEcuDumpModifyView(UserPassesTestMixin, FormView):
    form_class = OrgEcuDumpModifyForm
    template_name = 'ess/org_ecu_dump_upload.html'
    success_url = reverse_lazy('org-ecu-list')

    def test_func(self):
        return self.request.user.is_superuser

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            org_ecu_dump = OrgEcuDump.objects.get(pk=self.kwargs['pk'])
            if form.cleaned_data['sw']:
                org_ecu_dump.sw = form.cleaned_data['sw']
            if form.cleaned_data['hw']:
                org_ecu_dump.hw = form.cleaned_data['hw']
            if form.cleaned_data['ecu']:
                org_ecu_dump.ecu = form.cleaned_data['ecu']
            if form.cleaned_data['file']:
                org_ecu_dump.size = form.cleaned_data['file'].size
                org_ecu_dump.file = OrgEcuDumpPayload.objects.create(
                    content=form.cleaned_data['file'].read())
            org_ecu_dump.save()
            return self.form_valid(form)
        return self.form_invalid(form)

    def get_initial(self):
        org_ecu_dump = OrgEcuDump.objects.select_related().get(pk=self.kwargs['pk'])
        return {'sw': org_ecu_dump.sw, 'hw': org_ecu_dump.sw, 'ecu': org_ecu_dump.ecu}


class OrgEcuDumpDeleteView(UserPassesTestMixin, DeleteView):
    model = OrgEcuDump
    success_url = reverse_lazy('org-ecu-list')

    def test_func(self):
        return self.request.user.is_superuser


class EcuDumpModifyView(LoginRequiredMixin, FormView):
    form_class = EcuDumpModifyForm
    template_name = 'ess/ecu_dump_modify.html'
    success_url = reverse_lazy('file-list')

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            ecu_dump = EcuDump.objects.get(pk=self.kwargs['pk'])
            content = ecu_dump.file.content
            script_messages = ''
            modifications_made = ''
            for mod_script in ecu_dump.ecu.modscript_set.filter(sw=ecu_dump.sw):
                if form.cleaned_data.get(mod_script.modification.name):
                    temp_env = {}
                    exec(mod_script.script, temp_env)
                    content, script_message, error = temp_env['make_change'](content, ecu_dump.sw.name)
                    script_messages += script_message + '\n'
                    if error:
                        messages.error(request, script_message)
                    else:
                        messages.success(request, script_message)
                        modifications_made += mod_script.modification.name
            EcuDump.objects.create(user=request.user, ecu=ecu_dump.ecu, sw=ecu_dump.sw, parent=ecu_dump,
                                   name="{}_{}".format(ecu_dump.name, modifications_made), modified=True,
                                   file=EcuDumpPayload.objects.create(content=content),
                                   messages=script_messages.strip())
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        self.ecu_dump = EcuDump.objects.select_related().get(pk=self.kwargs['pk'])
        mod_scripts = self.ecu_dump.ecu.modscript_set.filter(sw=self.ecu_dump.sw).select_related()
        kwargs['mods'] = [mod_script.modification for mod_script in mod_scripts]

        return kwargs

    def get_context_data(self, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        kwargs['ecu_message'] = self.ecu_dump.ecu.message
        return kwargs


class ECuDumpDownloadView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        if request.user.is_superuser:
            ecu_dump = get_object_or_404(EcuDump, pk=self.kwargs['pk'])
        else:
            ecu_dump = get_object_or_404(EcuDump, pk=self.kwargs['pk'], user=request.user)
        filename = ecu_dump.name
        response = HttpResponse(content_type='application/octet-stream', content=ecu_dump.file.content)
        response["Content-Disposition"] = "attachment; filename={}".format(filename)
        return response


class OrgListFormView(LoginRequiredMixin, FormView):
    form_class = OrgEcuDumpSearchForm
    template_name = 'ess/org_ecu_dump_list.html'
    success_url = reverse_lazy('file-list')

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            kwargs = self.get_context_data()
            query = kwargs['orgs']
            hw = form.cleaned_data['hw'].strip()
            sw = form.cleaned_data['sw'].strip()
            brand = form.cleaned_data['brand'].strip()
            chassis = form.cleaned_data['chassis'].strip()
            engine = form.cleaned_data['engine'].strip()
            if hw:
                query = query.filter(hw__icontains=hw)
            if sw:
                query = query.filter(sw__icontains=sw)
            if brand:
                query = query.filter(ecu__engine__chassis__brand__name__icontains=brand)
            if chassis:
                query = query.filter(ecu__engine__chassis__name__icontains=chassis)
            if engine:
                query = query.filter(ecu__engine__name__icontains=engine)
            kwargs['orgs'] = query
            return self.render_to_response(kwargs)
        return self.form_invalid(form)

    def get_context_data(self, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        kwargs['orgs'] = OrgEcuDump.objects.defer('file').select_related().order_by(
            'ecu__engine__chassis__brand__name', 'ecu__engine__chassis__name', 'ecu__engine__name', 'ecu__name').all()
        return kwargs


class OrgEcuDumpDownloadView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        dump = get_object_or_404(OrgEcuDump.objects.select_related(), pk=self.kwargs['pk'])
        response = HttpResponse(content_type='application/octet-stream', content=dump.file.content)
        response["Content-Disposition"] = "attachment; filename={}-{}-{}-{}-{}-{}.org".format(
            dump.ecu.engine.chassis.brand.name,
            dump.ecu.engine.chassis.name,
            dump.ecu.engine.name,
            dump.ecu.name,
            dump.sw,
            dump.hw
        )
        return response
