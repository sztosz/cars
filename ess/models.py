from django.contrib.auth.models import User
from django.db import models


class Brand(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name


class Chassis(models.Model):
    brand = models.ForeignKey(Brand)
    name = models.CharField(max_length=50)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return "{} / {}".format(self.brand, self.name)


class Engine(models.Model):
    chassis = models.ForeignKey(Chassis)
    name = models.CharField(max_length=50)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return "{} / {}".format(self.chassis, self.name)


class EcuMessage(models.Model):
    message = models.TextField()

    def __str__(self):
        return self.message[:50]


class ECU(models.Model):
    engine = models.ForeignKey(Engine)
    name = models.CharField(max_length=50)
    message = models.ForeignKey(EcuMessage, null=True, blank=True)

    class Meta:
        ordering = ['-pk']

    def __str__(self):
        return "{} / {}".format(self.engine, self.name)


class Modification(models.Model):
    name = models.CharField(max_length=10)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name


class SoftwareVersion(models.Model):
    name = models.CharField(max_length=20)
    ecu = models.ForeignKey(ECU)

    def __str__(self):
        return '{} / {}'.format(self.ecu, self.name)

    class Meta:
        unique_together = ('name', 'ecu',)


class ModScript(models.Model):
    ecu = models.ManyToManyField(ECU)
    sw = models.ManyToManyField(SoftwareVersion)
    modification = models.ForeignKey(Modification)
    name = models.CharField(max_length=50)
    script = models.TextField()

    def __str__(self):
        return self.name


class EcuDumpPayload(models.Model):
    content = models.BinaryField()


class EcuDump(models.Model):
    user = models.ForeignKey(User)
    ecu = models.ForeignKey(ECU)
    name = models.CharField(max_length=50)
    sw = models.ForeignKey(SoftwareVersion)
    modified = models.BooleanField()
    parent = models.ForeignKey('self', null=True, blank=True)
    file = models.OneToOneField(EcuDumpPayload)
    messages = models.TextField(null=True, blank=True)
    date = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-date']

    def message_list(self):
        return self.messages.split('\n')

    def __str__(self):
        return self.name


class OrgEcuDumpPayload(models.Model):
    content = models.BinaryField()


class OrgEcuDump(models.Model):
    ecu = models.ForeignKey(ECU)
    sw = models.CharField(max_length=50, blank=True)
    hw = models.CharField(max_length=50, blank=True)
    file = models.OneToOneField(OrgEcuDumpPayload)
    date = models.DateTimeField(auto_now_add=True)
    size = models.IntegerField()

    class Meta:
        ordering = ['-date']


class ValidationScript(models.Model):
    ecu = models.OneToOneField(ECU)
    name = models.CharField(max_length=50)
    script = models.TextField()

    def __str__(self):
        return self.name
