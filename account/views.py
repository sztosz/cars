from io import BytesIO
from zipfile import ZipFile

from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.db.models import Prefetch
from django.http import HttpResponse
from django.utils import timezone
from django.views.generic import ListView, View

from ess.models import EcuDump, ModScript, ValidationScript


class EcuDumpListView(LoginRequiredMixin, ListView):
    model = EcuDump
    template_name = 'accounts/ecudump_list.html'

    def get_queryset(self):
        return EcuDump.objects.defer('file').filter(
            user=self.request.user,
            modified=False).select_related().prefetch_related(
                Prefetch('ecudump_set', queryset=EcuDump.objects.defer('file').select_related()))


class ScriptsDumpView(UserPassesTestMixin, View):
    def test_func(self):
        return self.request.user.is_superuser

    def get(self, request, *args, **kwargs):
        mod_scripts = ModScript.objects.all()
        validation_scripts = ValidationScript.objects.all()
        file = BytesIO()
        zip_file = ZipFile(file, 'w')
        for script in mod_scripts:
            zip_file.writestr('MODS/{}.py'.format(script.name), bytes(script.script, 'utf-8'))
        for validation_script in validation_scripts:
            zip_file.writestr('VALIDATION/{}_{}.py'.format(validation_script.ecu, validation_script.name),
                              bytes(validation_script.script, 'utf-8'))
        zip_file.close()
        response = HttpResponse(content_type='application/octet-stream', content=file.getvalue())
        response["Content-Disposition"] = "attachment; filename=scripts_backup_{}.zip".format(timezone.now())
        return response
