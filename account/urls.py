from django.conf.urls import url

from account.views import EcuDumpListView, ScriptsDumpView

urlpatterns = [
    url('^file_list/$',
        EcuDumpListView.as_view(),
        name='file-list'),
    url('^script_dump/$',
        ScriptsDumpView.as_view(),
        name='script-dump'),
]
